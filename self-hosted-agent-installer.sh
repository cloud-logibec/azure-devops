#!/bin/bash

#####
# Show usage
#####
function usage()
{
    echo
    echo "Comands:"
    echo "  install                              Install all the required components for the Azure DevOps agents"
    echo
    echo "Options:"
    echo "  -h, --help                           Show usage"
    echo "  --proxy-protocol                     HTTP(S) proxy protocol (http or https)"
    echo "  --proxy-host                         HTTP(S) proxy host (host[:port])"
    echo "  --proxy-username                     HTTP(S) proxy username"
    echo "  --proxy-password                     HTTP(S) proxy password"

    if [ "$COMMAND" = "install" ]; then
      echo
      echo "  Required:"
      echo "    -pat, --personal-access-token      Personal access token"
      echo "    -p, --agent-pool                   Agent pool name"
      echo "    -n, --agent-name                   Agent name"
      echo
      echo "  Defaults:"
      echo "    -v, --agent-version                Agent version ($AZURE_DEVOPS_AGENT_VERSION)"
      echo "    -o, --organization                 Azure Devops organization ($AZURE_DEVOPS_ORGANIZATION)"
      echo "    -u, --user                         Service account user ($AZURE_DEVOPS_USER)"
      echo "    -w, --working-dir                  Agent working directory ($AZURE_DEVOPS_WORKING_DIR)"
      echo "    -r, --replace                      Replace an exisiting agent"
      echo "    --skip-dependencies                Do not install dependencies in case they are already installed (false)"
    fi
    
    echo
    echo "Usage:"
    echo "  ./self-hosted-agent-installer.sh [command] [options]"
    echo "  ./self-hosted-agent-installer.sh --help"
    echo "  ./self-hosted-agent-installer.sh [command] --help"

    if [ "$COMMAND" = "install" ]; then
      echo "  ./self-hosted-agent-installer.sh install -pat <personal_access_token> -p <deployment_pool_name> -n <agent_name>" 
    fi

    echo
    exit 0
}


###
# Setup the http_proxy and https_proxy environment variables
# Must provide at least the proxy host
###
function set_proxy() {
  if [ ! -z "$AZURE_DEVOPS_PROXY_HOST" ] ; then
    # build proxy url <[protocol://][user:password@]proxyhost[:port]>
    proxy=""
    if [[ ! -z "$AZURE_DEVOPS_PROXY_USERNAME" ]] ; then 
      proxy="$AZURE_DEVOPS_PROXY_USERNAME" 
    fi
    if [[ ! -z "$AZURE_DEVOPS_PROXY_PASSWORD" ]] ; then 
      proxy="$proxy:$AZURE_DEVOPS_PROXY_PASSWORD@" 
    fi
    if [[ ! -z "$proxy" ]] ; then 
      proxy="$proxy@" 
    fi
    proxy="$proxy$AZURE_DEVOPS_PROXY_HOST"

    # Set global variables executable using those variables 
    export http_proxy="$AZURE_DEVOPS_PROXY_PROTOCOL://$proxy"
    export https_proxy="$AZURE_DEVOPS_PROXY_PROTOCOL://$proxy"

    echo "Http proxy set to $AZURE_DEVOPS_PROXY_PROTOCOL://$proxy"
  fi
} 

function install() {
  
  error_msg=0

  # check required variables
  [[ -z "$AZURE_DEVOPS_PERSONAL_ACCESS_TOKEN" ]] && error_msg=1
  [[ -z "$AZURE_DEVOPS_AGENT_POOL" ]] && error_msg=1
  [[ -z "$AZURE_DEVOPS_AGENT_NAME" ]] && error_msg=1
  [[ "$error_msg" -eq 1 || "$HELP" -eq 1 ]] && usage

  # passwordless sudo is required to be able to install stuff from Azure DevOps pipelines
  echo "Grant passwordless sudo to $AZURE_DEVOPS_USER"
  echo "$AZURE_DEVOPS_USER ALL=(ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers.d/z-$AZURE_DEVOPS_USER
  
  # set proxy
  set_proxy
  
  # create a folder to install and extract the agent
  [ ! -d "vsts-agent" ] && mkdir vsts-agent
  cd vsts-agent

  # create the working directory for the agent and set the owner and permissions
  # the user must provider a full path to avoid sudo conflicts
  if [[ "$AZURE_DEVOPS_WORKING_DIR" != "_work" ]] && [[ ! -d "$AZURE_DEVOPS_WORKING_DIR" ]] ; then 
    if [[ ! $AZURE_DEVOPS_WORKING_DIR = /* ]] ; then 
       echo "Working directory must be a full path"
       exit 1
    fi
    sudo mkdir -p $AZURE_DEVOPS_WORKING_DIR
    sudo chown $AZURE_DEVOPS_USER:$AZURE_DEVOPS_USER $AZURE_DEVOPS_WORKING_DIR
    sudo chmod 0755 $AZURE_DEVOPS_WORKING_DIR
  fi

  # try both curl or wget to download the agent and extract the agent package
  # exit if it fails
  echo "Downloading and extracting agent."
  if [ -x "$(command -v curl)" ]; then
    curl $AZURE_DEVOPS_AGENT_URL | tar xz  
  elif [ -x "$(command -v wget)" ] ; then
    wget $AZURE_DEVOPS_AGENT_URL -O - | tar xz
  else
    echo "curl or wget not availalble"
    exit 1
  fi
  
  # There is some dependencies required to run the agent. 
  # These dependencies might be installted already so this flag won't install them
  if [ "$AZURE_DEVOPS_SKIP_DEPENDENCIES" -eq 0 ]; then
    echo "Install agent dependencies"
    sudo ./bin/installdependencies.sh 
  fi
  
  # Run config.sh which setup the agent and generate the svc.sh 
  # config_cmd="./config.sh --unattended --auth pat --token $AZURE_DEVOPS_PERSONAL_ACCESS_TOKEN --deploymentpool --deploymentpoolname $AZURE_DEVOPS_AGENT_POOL --agent $AZURE_DEVOPS_AGENT_NAME --url $AZURE_DEVOPS_ORGANIZATION --acceptTeeEula --work $AZURE_DEVOPS_WORKING_DIR --addDeploymentGroupTags --deploymentGroupTags $AZURE_DEVOPS_AGENT_NAME"
  config_cmd="./config.sh --unattended --auth pat --token $AZURE_DEVOPS_PERSONAL_ACCESS_TOKEN --pool $AZURE_DEVOPS_AGENT_POOL --agent $AZURE_DEVOPS_AGENT_NAME --url $AZURE_DEVOPS_ORGANIZATION --acceptTeeEula --work $AZURE_DEVOPS_WORKING_DIR"
  
  # Add proxy arguments
  [[ ! -z "$AZURE_DEVOPS_PROXY_HOST" ]] && config_cmd="$config_cmd --proxyurl $AZURE_DEVOPS_PROXY_PROTOCOL://$AZURE_DEVOPS_PROXY_HOST "
  [[ ! -z "$AZURE_DEVOPS_PROXY_USERNAME" ]] && config_cmd="$config_cmd --proxyusername $AZURE_DEVOPS_PROXY_USERNAME "
  [[ ! -z "$AZURE_DEVOPS_PROXY_PASSWORD" ]] && config_cmd="$config_cmd --proxypassword $AZURE_DEVOPS_PROXY_PASSWORD "
  
  # execute the config.sh command
  eval ${config_cmd}
  
  # install the agent as a service
  sudo ./svc.sh install $AZURE_DEVOPS_USER
  sudo ./svc.sh start
}


# Handler scritps arguments
ARGS_COUNT="$#"
COMMAND="install"
HELP=0
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -pat|--personal-access-token)
    AZURE_DEVOPS_PERSONAL_ACCESS_TOKEN="$2"
    shift
    shift
    ;;
    -p|--agent-pool)
    AZURE_DEVOPS_AGENT_POOL="$2"
    shift
    shift
    ;;
    -n|--agent-name)
    AZURE_DEVOPS_AGENT_NAME="$2"
    shift
    shift
    ;;
    -v|--agent-version)
    AZURE_DEVOPS_AGENT_VERSION="$2"
    shift
    shift
    ;;
    -o|--organization)
    AZURE_DEVOPS_ORGANIZATION="$2"
    shift
    shift
    ;;
    -u|--user)
    AZURE_DEVOPS_USER="$2"
    shift
    shift
    ;;
    -w|--working-dir)
    AZURE_DEVOPS_WORKING_DIR="$2"
    shift
    shift
    ;;
    --skip-dependencies)
    AZURE_DEVOPS_SKIP_DEPENDENCIES=1
    shift
    ;;
    --proxy-protocol)
    AZURE_DEVOPS_PROXY_PROTOCOL="$2"
    shift
    shift
    ;;
    --proxy-host)
    AZURE_DEVOPS_PROXY_HOST="$2"
    shift
    shift
    ;;
    --proxy-username)
    AZURE_DEVOPS_PROXY_USERNAME="$2"
    shift
    shift
    ;;
    --proxy-password)
    AZURE_DEVOPS_PROXY_PASSWORD="$2"
    shift
    shift
    ;;
    install)
    COMMAND="install"
    shift
    ;;
    -h|--help|*)
    POSITIONAL+=("$1")
    HELP=1
    shift
    ;;
esac
done

set -- "${POSITIONAL[@]}" # restore positional parameters
set -e 

# Initialize script variables
AZURE_DEVOPS_PERSONAL_ACCESS_TOKEN=${AZURE_DEVOPS_PERSONAL_ACCESS_TOKEN:=""}
AZURE_DEVOPS_AGENT_POOL=${AZURE_DEVOPS_AGENT_POOL:=""}
AZURE_DEVOPS_AGENT_NAME=${AZURE_DEVOPS_AGENT_NAME:=""}

AZURE_DEVOPS_AGENT_VERSION=${AZURE_DEVOPS_AGENT_VERSION:="2.155.1"}
AZURE_DEVOPS_ORGANIZATION=${AZURE_DEVOPS_ORGANIZATION:="https://dev.azure.com/keycloak"}
AZURE_DEVOPS_AGENT_URL=${AZURE_DEVOPS_AGENT_URL:="https://vstsagentpackage.azureedge.net/agent/$AZURE_DEVOPS_AGENT_VERSION/vsts-agent-linux-x64-$AZURE_DEVOPS_AGENT_VERSION.tar.gz"}
AZURE_DEVOPS_USER=${AZURE_DEVOPS_USER:="$USER"}
AZURE_DEVOPS_WORKING_DIR=${AZURE_DEVOPS_WORKING_DIR:="_work"}
AZURE_DEVOPS_SKIP_DEPENDENCIES=${AZURE_DEVOPS_SKIP_DEPENDENCIES:=0}
AZURE_DEVOPS_PROXY_PROTOCOL=${AZURE_DEVOPS_PROXY_PROTOCOL:="http"}
AZURE_DEVOPS_PROXY_HOST=${AZURE_DEVOPS_PROXY_HOST:=""}
AZURE_DEVOPS_PROXY_USERNAME=${AZURE_DEVOPS_PROXY_USERNAME:=""}
AZURE_DEVOPS_PROXY_PASSWORD=${AZURE_DEVOPS_PROXY_PASSWORD:=""}

#  if no args show usage
if [ "$ARGS_COUNT" -eq 0 ]; then
  usage
fi

# run commands 
case $COMMAND in
  # install function
  "install") install;;
  # show usage if now commands
  *) usage;;
esac