# Azure DevOps Self-Hosted agent installer

## How to install

* Obtains a [Personal Access Token (PAT)](https://docs.microsoft.com/en-us/azure/devops/pipelines/agents/v2-linux?view=azure-devops#permissions)
* You setup Deployment pool in Azure DevOps
  * [Deployment groups](https://docs.microsoft.com/en-us/azure/devops/pipelines/release/deployment-groups/?view=azure-devops)

* Install from a server

```bash
curl https://bitbucket.org/cloud-logibec/azure-devops/raw/HEAD/self-hosted-agent-installer.sh | bash -s -- [args]

# curl https://bitbucket.org/cloud-logibec/azure-devops/raw/HEAD/self-hosted-agent-installer.sh | bash -s -- install -pat <personal_access_token> -p <agent_pool_name> -n <agent_name>
```

## Usage

```text
Comands:
  install                              Install all the required components for the Azure DevOps agents

Options:
  -h, --help                           Show usage
  --proxy-protocol                     HTTP(S) proxy protocol (http or https)
  --proxy-host                         HTTP(S) proxy host (host[:port])
  --proxy-username                     HTTP(S) proxy username
  --proxy-password                     HTTP(S) proxy password

  Required:
    -pat, --personal-access-token      Personal access token
    -p, --agent-pool                   Agent pool name
    -n, --agent-name                   Agent name

  Defaults:
    -v, --agent-version                Agent version (2.155.1)
    -o, --organization                 Azure Devops organization (https://dev.azure.com/keycloak)
    -u, --user                         Service account user (chaale01)
    -w, --working-dir                  Agent working directory (_work)
    -r, --replace                      Replace an exisiting agent
    --skip-dependencies                Do not install dependencies in case they are already installed (false)

Usage:
  ./self-hosted-agent-installer.sh [command] [options]
  ./self-hosted-agent-installer.sh --help
  ./self-hosted-agent-installer.sh [command] --help
  ./self-hosted-agent-installer.sh install -pat <personal_access_token> -p <deployment_pool_name> -n <agent_name>
```

## What's missing

* Test install behind proxy
* Support for agent pools
